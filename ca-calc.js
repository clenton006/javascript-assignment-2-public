// Nice work here. Well commented, and the basic functionality
// works great. I did find a couple of bugs:
// - When a user clicks on an operation sign and then another
// operation sign, I get 'NaN'
// - When the user clicks on an operation and then the '='
// I also get 'NaN'
// Otherwise everything looked great.  Design was outstanding.

memory = "0"; // initialise memory variable
Current = "0"; //   and value of Display ("current" value)
Operation = 0; // Records code for eg * / etc.
MAXLENGTH = 30; // maximum number of digits before decimal!

function addDigit(digit) { //ADD A DIGIT TO DISPLAY (kept as 'Current')
		if (Current.length > MAXLENGTH) {
				Current = "Aargh! Too long"; //limit length
		} else {
				if ((eval(Current) == 0) && (Current.indexOf(".") == -1)) {
						Current = digit;
				} else {
						Current = Current + digit;
				};
		};
		document.Calculator.Display.value = Current;
};

function Dot() {
		if (Current.length == 0) {
				Current = "0.";
		} else {
				// Current.indexOf(".") returns 0 or 1 if there IS a '.' in Current
				// to compare for no '.', use -1
				if (Current.indexOf(".") == -1) {
						Current = Current + ".";
				};
		};
		document.Calculator.Display.value = Current;
}

// function Clear() {
// 		Current = Memory;
// 		document.Calculator.Display.value = Current;
// }

function AllClear() {
		Current = "0";
		Operation = 0;
		Memory = Current;
		document.Calculator.Display.value = Current;
}

function Operate(op) { //STORE OPERATION e.g. + * / etc.

		if (op.indexOf("*") > -1) {
				Operation = 1;
		}; //codes for *
		if (op.indexOf("/") > -1) {
				Operation = 2;
		}; // slash (divide)
		if (op.indexOf("+") > -1) {
				Operation = 3;
		}; // sum
		if (op.indexOf("-") > -1) {
				Operation = 4;
		}; // difference

		Memory = Current; //store value
		Current = ""; //or we could use "0"
		document.Calculator.Display.value = Current;
}

function Calculate() { //PERFORM CALCULATION (= button)
		if (Operation == 1) {   // multiplication because of what happened in operate()
				Current = eval(Memory) * eval(Current);
		};
		if (Operation == 2) {
			if (Current != 0){ //if the second number (Current) is not 0 then divide as normal
				Current = eval(Memory) / eval(Current);
			}else{						//division by zero will return an "Error" message instead of NaN (0/0) or Infinity (non-zero/zero)
				Current= "Error";
			}
		};

		if (Operation == 3) {
				Current = eval(Memory) + eval(Current);
		};
		if (Operation == 4) {
				Current = eval(Memory) - eval(Current);
		};
		Operation = 0; //clear operation
		Memory = "0"; //clear memory
		document.Calculator.Display.value = Current;
}
