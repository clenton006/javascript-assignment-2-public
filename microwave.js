// Nice looking microwave.  It works well and has nicely
// commented code.  I did find a bug:
// - when a user enters a number, lets it count down a few seconds,
// then clicks cancel.  While the stopped number is displayed, the
// user clicks on several of the number and buttons, then
// starts the countdown again, sometimes the countdown will skip numbers
// right through to where it left off - up to 10 or 20 numbers at a time.
// Otherwise everything looked great.  Great teamwork.  Your team had a
// total of 58 commits.
// Calculator - 8/10
// Microwave  - 9/10
// Design     - 10/10
// Total      - 27/30

$(function(){
        //loads the default display into the microvave
        var time = ["0","0",":","0","0"];
        var timeOutputDefault = "";
        var output=[];
        var timerSec;
        var timerMin;
        //some buttons are disabled by default
        document.getElementById("start").disabled=true;
        document.getElementById("stop").disabled=true;
        document.getElementById("clear").disabled=true;
        document.getElementById("zero").disabled=true;
        //loads the array into a string to show on the ouotput
        for(i=0;i<time.length; i++){
            timeOutputDefault += time[i];
        }

        document.getElementById("outputTimer").innerHTML = timeOutputDefault;
        //adds the number into the array and over writes the numbers if more that 4 are pressed.
    $(".numbers").click(function(){
        //enables some buttons when a time is being entered
        document.getElementById("start").disabled=false;
        document.getElementById("clear").disabled=false;
        document.getElementById("zero").disabled=false;
        //shifts all of the values of the aray over one in the display
        time[0] = time[1];
        time[1] = time[3];
        time[3] = time[4];
        time[4] = (this.value);
        //takes the first 5 values from the array to display
        //this is used because when ever a button is presseed it adds 5 new values
        //into the array but we only want to show 5 of them
        output = time.slice(0,5);
        timeOutputDefault="";
        //redisplays the new values as they are entered
        for(i=0;i<output.length; i++){
            timeOutputDefault += output[i];
        }

        document.getElementById("outputTimer").innerHTML = timeOutputDefault;
    });
        // TIMER GLOBAL VARIABLES
        //these variables will keep track of the minutes and seconds independantly.
        var min = "";
        var sec = "";
        //keps track of the first time
        var initialsec;
        var initialmin;
        //a counter so it only stores the initial values once
        var startCount=0;
        //this will start the countdown
    $("#start").click(function(){
        //gets the values from the array and uses those numbers to create a value for minutes and seconds
        min = time[0] + time[1];
        sec = time[3] + time[4];
        //stores the entered values once
        //will be used if the timer is stopped and started again
        if(startCount<1){
            initialsec = sec;
            initialmin = min;
            startCount++;
        }
        //changes the values of min and sec int\o a digit instead of a string
        min = parseInt(min);
        sec = parseInt(sec);
        //changes the state of buttons again when start is pressed
        document.getElementById("start").disabled=true;
        document.getElementById("stop").disabled=false;
        document.getElementById("clear").disabled=true;
        //this will disable all of the numbered buttons while the timer is running so the user cannot enter new values
        disableNumbers();
        //displays the time
        timeOutputDefault = "";
        timeoutputDefault = min + ":" + sec;
        document.getElementById("outputTimer").innerHTML = timeOutputDefault;
        //will take the values and take them out to start the timer
        initialize(min, sec);

    });

        //this will pause the countdown
    $("#stop").click(function(){
        //these equations are used to track the timer value as the clock counts down
        //we are collecting information on how many times it has gone(how many seconds have passed)
        //this one takes the remainder from dividing by 60
        timerSec = timer % 60;
        //this one tracks to see if a full minute has passed or not
        timerMin = timer / 60;
        //will round down to grab a minute passed value
        timerMin = Math.floor(timerMin);
        //checks to see if the initial second value was zero
        if(initialsec=="00"){
            //sets a new initial valie to not throw an error
            initialsec=61;
            //converts min into an integer
            initialmin = parseInt(initialmin);
            //decreases one minute
            initialmin = initialmin-1;
        }
        //sets both of the values by subtracting the initial values by the time passed
        sec = initialsec - timerSec;
        min = initialmin - timerMin;
        //checks to see if the subtraction makes sec go below and changes the numbers to fix this issue
        if(sec<0){
            //adds 60 seconds onto sec
            sec = sec + 60;
            //takes away a minute for adding 60 seconds
            min--;
        }
        //puts the sec and min back to a string
        sec = sec.toString();
        min = min.toString();
        //records the new time values to display as the timer is paused
        time[0] = min.charAt(0);
        time[1] = min.charAt(1);
        time[3] = sec.charAt(0);
        time[4] = sec.charAt(1);
        //actually stops the timer
        clearTimeout(timer);
        //changes the state of buttons so you can cancel or start again while its paused
        document.getElementById("start").disabled=false;
        document.getElementById("clear").disabled=false;
        //resets the minute checker counter back to zero
        resetCounter();
        //stops running
        return false;


    });
        //this will reset the timer
        //over writes the previous array and sets the values back to 0
    $("#clear").click(function(){
        //lets you enter number values again
        enableNumbers();
        //resets the initalize counter back to zero so it will run once the timer is started again
        resetStartCounter(startCount);
        //puts buttons back to default state
        document.getElementById("start").disabled=true;
        document.getElementById("stop").disabled=true;
        document.getElementById("zero").disabled=true;
        //sets the display to show 00:00
        timeOutputDefault = "";
        time = ["0","0",":","0","0"];
        for(i=0;i<time.length; i++){
            timeOutputDefault += time[i];
        }

        document.getElementById("outputTimer").innerHTML = timeOutputDefault;

    });
});
    //these variables are used to begin the timer
    var timer;
    var min;
    var sec;
    var counter = 0;
    //starts timer
    function initialize(minEntered, secEntered){
    //sets the values of min and sec from what was entered and passed through
        min = minEntered;
        sec = secEntered;
        //starts timer and will run repeatedly
        startTimer();
    }
    function resetCounter(){
        //sets the counter that checks the minute is less than ten value back to zero
        counter = 0;
    }
    function resetStartCounter(startCount){
        //sets the counter that gets teh initial values back to zero
        startCount = 0;
    }
    //starts everything
    function startTimer(){
        //makes sure that secinds is an integer
        sec = parseInt(sec);
        //checks to see if any of the values are less than zero
        if(sec<0 || min<0){
            //if any values are below zero an error message will be displayed
            timeOutputDefault = "ERROR";
            // you can clear and enter another value
            document.getElementById("clear").disabled=false;
            document.getElementById("outputTimer").innerHTML = timeOutputDefault;
            return false;
        }
        //checks to see if \seconds will be below ten
        if(sec<10){
            //if it is it will ad a zero in front of the display value of seconds
            sec = sec.toString();
            sec = "0" + sec;
        }
        //this checks to see if seconds equal zero
        if(sec==0){
            //if it does it will display this value
            sec="00";
        }
        //this checks to see if minutes is below zero
        // this will only run once for each time the timer is reset because it will keep adding zeros every second if it runs multiple times
        //once minutes is below 10 once it will stay a single digit til its zero or reset
        if(min<10 && counter<1){
            min = min.toString();
            min = "0" + min;
            counter++;
        }
        //checks to see if min is equal to zero
        if(min==0){
            min = "00";
        }
        // this will run only when both numbers equal zero which will only happen when the timer is complete
        if(min=="00" && sec=="00"){
            // this cahnges the state of some buttons so you can only clear
            //when you clear, the buttons will be at default again
            document.getElementById("start").disabled=true;
            document.getElementById("stop").disabled=true;
            document.getElementById("clear").disabled=false;
            //this will showw DO:NE as a display
            timeOutputDefault = "DO:NE";
            document.getElementById("outputTimer").innerHTML = timeOutputDefault;
            //will run a command to make the display flash
            animate();
            return false;
        }
        //displays the value after checking all of the possible imput and checking for errors
        timeOutputDefault = "";
        timeOutputDefault = min + ":" + sec;
        document.getElementById("outputTimer").innerHTML = timeOutputDefault;
        //if sec equals zero, it will handle how the min and seconds will react
        if(sec=="00"){
            //this will put seconds back up to 60 and will start counting dowbn again
            sec = 60;
            //the minute value will go down one as well
            min--;
            //will reset counter the check again if minute is below ten
            counter = 0;
        }
        //decreases one second everytime the timer runs
        sec = sec -1;
        //timer is set to 1000ms
        timer = setTimeout("startTimer()", 1000);
        }
//disables all numbered buttons
    function disableNumbers(){
        document.getElementById("one").disabled=true;
        document.getElementById("two").disabled=true;
        document.getElementById("three").disabled=true;
        document.getElementById("four").disabled=true;
        document.getElementById("five").disabled=true;
        document.getElementById("six").disabled=true;
        document.getElementById("seven").disabled=true;
        document.getElementById("eight").disabled=true;
        document.getElementById("nine").disabled=true;
        document.getElementById("zero").disabled=true;
        }
//enables all numbered buttons
    function enableNumbers(){
        document.getElementById("one").disabled=false;
        document.getElementById("two").disabled=false;
        document.getElementById("three").disabled=false;
        document.getElementById("four").disabled=false;
        document.getElementById("five").disabled=false;
        document.getElementById("six").disabled=false;
        document.getElementById("seven").disabled=false;
        document.getElementById("eight").disabled=false;
        document.getElementById("nine").disabled=false;
        document.getElementById("zero").disabled=false;
        }
    function animate(){
        //this will cause the done text to fade in and out as if it was blinking
        $("#outputTimer").fadeOut("slow")
                        .fadeIn("slow")
                        .fadeOut("slow")
                        .fadeIn("slow")
                        .fadeOut("slow")
                        .fadeIn("slow");
    }
//stop and start with seconds over 60 seconds
//can handle it using if sec<60
//may fix later
